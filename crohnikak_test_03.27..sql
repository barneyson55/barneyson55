-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2022. Már 27. 12:08
-- Kiszolgáló verziója: 10.4.19-MariaDB
-- PHP verzió: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `crohnikak`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `allergenek`
--

CREATE TABLE `allergenek` (
  `id` int(10) UNSIGNED NOT NULL,
  `allergen_nev` varchar(100) DEFAULT NULL,
  `allergen_created` timestamp NULL DEFAULT current_timestamp(),
  `allergen_modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `etelek`
--

CREATE TABLE `etelek` (
  `id` int(10) UNSIGNED NOT NULL,
  `etel_nev` varchar(100) DEFAULT NULL,
  `etel_created` timestamp NULL DEFAULT current_timestamp(),
  `etel_modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `etel_tapertek` int(11) DEFAULT NULL,
  `etel_menny` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `etkezesek`
--

CREATE TABLE `etkezesek` (
  `id` int(10) UNSIGNED NOT NULL,
  `etk_nev` varchar(100) DEFAULT NULL,
  `etk_created` timestamp NULL DEFAULT current_timestamp(),
  `etk_modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyogyszerek`
--

CREATE TABLE `gyogyszerek` (
  `id` int(10) UNSIGNED NOT NULL,
  `gyszer_nev` varchar(100) DEFAULT NULL,
  `gyszer_created` timestamp NULL DEFAULT current_timestamp(),
  `gyszer_modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `gyszer_menny` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `italok`
--

CREATE TABLE `italok` (
  `id` int(10) UNSIGNED NOT NULL,
  `ital_nev` varchar(100) DEFAULT NULL,
  `etel_created` timestamp NULL DEFAULT current_timestamp(),
  `ital_modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `etel_tapertek` int(11) DEFAULT NULL,
  `ital_menny` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `naplo_entryk`
--

CREATE TABLE `naplo_entryk` (
  `id` int(10) UNSIGNED NOT NULL,
  `entry_nev` varchar(100) DEFAULT NULL,
  `entry_created` timestamp NULL DEFAULT current_timestamp(),
  `entry_modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `napszakok`
--

CREATE TABLE `napszakok` (
  `id` int(10) UNSIGNED NOT NULL,
  `napszak_nev` varchar(100) DEFAULT NULL,
  `napszak_created` timestamp NULL DEFAULT current_timestamp(),
  `napszak_modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `panaszok`
--

CREATE TABLE `panaszok` (
  `id` int(10) UNSIGNED NOT NULL,
  `panasz_nev` varchar(100) DEFAULT NULL,
  `panasz_created` timestamp NULL DEFAULT current_timestamp(),
  `panasz_modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `panasz_mertek` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `allergenek`
--
ALTER TABLE `allergenek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `etelek`
--
ALTER TABLE `etelek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `etkezesek`
--
ALTER TABLE `etkezesek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyogyszerek`
--
ALTER TABLE `gyogyszerek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `italok`
--
ALTER TABLE `italok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `naplo_entryk`
--
ALTER TABLE `naplo_entryk`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `napszakok`
--
ALTER TABLE `napszakok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `panaszok`
--
ALTER TABLE `panaszok`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `allergenek`
--
ALTER TABLE `allergenek`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `etelek`
--
ALTER TABLE `etelek`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `etkezesek`
--
ALTER TABLE `etkezesek`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `gyogyszerek`
--
ALTER TABLE `gyogyszerek`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `italok`
--
ALTER TABLE `italok`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `naplo_entryk`
--
ALTER TABLE `naplo_entryk`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `napszakok`
--
ALTER TABLE `napszakok`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `panaszok`
--
ALTER TABLE `panaszok`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
